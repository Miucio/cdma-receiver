LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY CDMA IS
	generic(
		Nbit  : integer
	);
	port(
		chip_stream    	 : in std_logic;
		code_word    	 : in std_logic;
		clk	    	 : in std_logic;
		res		 : in std_logic;
		res_comp	 : in std_logic;
		bit_stream	 : out std_logic
	);
END CDMA;

ARCHITECTURE bh OF CDMA IS
	signal out_chip 	: std_logic_vector(Nbit-1 downto 0);	
	signal out_code 	: std_logic_vector(Nbit-1 downto 0);
	signal mubit		: std_logic;
	signal chip		: std_logic;	

	component SIPO is
	generic(
		Nbit_out : integer
	);
	port(
		sin     : in std_logic;
		clk 	: in std_logic;
		res 	: in std_logic;
		pout	: out std_logic_vector(Nbit-1 downto 0)
	);
	END component;

	component COMPARE IS
	generic(
		Nbit  : integer
	);
	port(
		chip_stream    	 : in std_logic_vector(Nbit-1 downto 0);
		code_word    	 : in std_logic_vector(Nbit-1 downto 0);
		clk	    	 : in std_logic;
		res		 : in std_logic;
		bit_stream	 : out std_logic
	);
	END component;

 	component MULTIP is
	generic(
		Nbit:  integer
  	 );
   	port( 
		O 	: in std_logic;
		P 	: in std_logic;
		clk     : in std_logic;
		res	: in std_logic;
      		Result  : out std_logic;
		chip	: out std_logic
   	);
	END component;

begin
	
	multi : MULTIP
	generic map(Nbit => Nbit)
	port map(chip_stream,code_word,clk,res,mubit,chip);

	sipo_chip : SIPO
	generic map(Nbit_out => Nbit)
	port map(chip,clk,res,out_chip); --using chip to coordinate
	
	sipo_code : SIPO
	generic map(Nbit_out => Nbit)
	port map(mubit,clk,res,out_code);
	
	comp : COMPARE
	generic map(Nbit => Nbit)
	port map(out_chip,out_code,clk,res_comp,bit_stream);
	
END bh;

