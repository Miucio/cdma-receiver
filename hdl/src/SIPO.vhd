LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY SIPO is 
   generic(
	Nbit_out: integer	
   );
   port(
      sin  : in std_logic;    
      clk  : in std_logic; 
      res  : in std_logic; 
      pout : out std_logic_vector(Nbit_out-1 downto 0)  
   );
END SIPO;
ARCHITECTURE sipo_beh of SIPO is
	signal Q: std_logic_vector(Nbit_out-1 downto 0);
begin
process(clk, res)
	begin
  	 if(res='1') then  -- IF RESET IS 1 SIGNAL 0000
	   Q <= (others => '0');
	 elsif (clk'event and clk ='1') then  --IF CLOCK IS 1 SET LAST BIT
	    Q(0)<=sin;
	    Q(1)<=Q(0);
	    Q(2)<=Q(1);
	    Q(3)<=Q(2);
	    Q(4)<=Q(3);
	    Q(5)<=Q(4);
	    Q(6)<=Q(5);
	    Q(7)<=Q(6);
	    Q(8)<=Q(7);
	    Q(9)<=Q(8);
	    Q(10)<=Q(9);
	    Q(11)<=Q(10);
	    Q(12)<=Q(11);
	    Q(13)<=Q(12);
	    Q(14)<=Q(13);
	    Q(15)<=Q(14);
	 end if;
	end process;
 pout<=Q;
END sipo_beh;
