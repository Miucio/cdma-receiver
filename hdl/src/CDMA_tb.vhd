LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY CDMA_tb IS
END CDMA_tb;

ARCHITECTURE bh OF CDMA_tb IS
	
	component CDMA_tb is
	generic(
			Nbit_in : integer;
			Nbit_out: integer
			);
	 port(
		 chip_stream	: in 	std_logic;
		 code_word	: in 	std_logic;
		 clk 		: in  	std_logic;
		 res		: in 	std_logic;
		 bit_stream	: out 	std_logic
		 );
	end component;
	
	constant T_CLK   : time := 20 ns; -- Clock period
	constant T_RESET : time := 5 ns; -- Period before the reset deassertion
	
			-- inputs
	signal res  		: std_logic := '1';
	signal res_comp		: std_logic := '1';
	signal chip_stream 	: std_logic := '0';
	signal code_word 	: std_logic := '0';
	signal clk  		: std_logic := '0'; -- clock signal, intialized to '0' 
	
	          --Outputs
	signal bit_stream : std_logic;
begin
    clk <= (not(clk) ) after (T_CLK) / 2;    --clock chip & code stream
    res <= '0' after T_RESET;
    res_comp <= '0' after T_CLK+T_RESET; --Period before the comparator will stable 
	
		cdmarec : entity work.CDMA
		generic map(Nbit  => 16)
		port map(
			chip_stream     => chip_stream,
			code_word       => code_word,
			clk	    	=> clk,
			res             => res,
			res_comp        => res_comp,
			bit_stream	=> bit_stream
		);
	--Original flow was 1001
  stim_proc: process
   begin   
-- flow 1                    
    	chip_stream<='1'; --bit 0
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 1
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 2
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 3
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 4
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 5
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 6
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 7
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 8
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 9
	code_word<='0';
    wait for 20 ns;
	chip_stream<='1'; --bit 20
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 11
	code_word<='0';
    wait for 20 ns;
	chip_stream<='1'; --bit 12
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 13
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 14
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 15
	code_word<='1';
    wait for 20 ns;

-- flow 0
	chip_stream<='1'; --bit 0
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 1
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 2
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 3
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 4
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 5
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 6
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 7
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 8
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 9
	code_word<='1';
    wait for 20 ns;
	chip_stream<='1'; --bit 20
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 11
	code_word<='1';
    wait for 20 ns;
	chip_stream<='1'; --bit 12
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 13
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 14
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 15
	code_word<='0';
    wait for 20 ns; 

-- flow 0
	chip_stream<='1'; --bit 0
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 1
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 2
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 3
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 4
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 5
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 6
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 7
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 8
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 9
	code_word<='1';
    wait for 20 ns;
	chip_stream<='1'; --bit 20
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 11
	code_word<='1';
    wait for 20 ns;
	chip_stream<='1'; --bit 12
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 13
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 14
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 15
	code_word<='0';
    wait for 20 ns;

-- flow 1
    	chip_stream<='1'; --bit 0
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 1
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 2
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 3
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 4
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 5
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='1'; --bit 6
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 7
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 8
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 9
	code_word<='0';
    wait for 20 ns;
	chip_stream<='1'; --bit 20
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 11
	code_word<='0';
    wait for 20 ns;
	chip_stream<='1'; --bit 12
	code_word<='1';
    wait for 20 ns;      
	chip_stream<='0'; --bit 13
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='0'; --bit 14
	code_word<='0';
    wait for 20 ns;      
	chip_stream<='1'; --bit 15
	code_word<='1';
    wait for 20 ns; 
     
 end process;

end bh;

