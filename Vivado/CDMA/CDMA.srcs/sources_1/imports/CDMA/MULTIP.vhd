LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;


ENTITY MULTIP is
	generic(
	Nbit:  integer
   );
   port( 
	O 	: in std_logic;
	P 	: in std_logic;
	clk     : in std_logic;
	res	: in std_logic;
      	Result  : out std_logic;
	chip	: out std_logic
   );
END MULTIP;

ARCHITECTURE multi_bh of MULTIP is
	
begin
 process(clk,res)
 begin
  if res = '1' then
     	Result <= '0';
	chip <= '0';
  elsif clk'event AND clk = '1' THEN
       Result <=  O AND P;
       chip <= O;
  end if;
 end process;
END multi_bh;