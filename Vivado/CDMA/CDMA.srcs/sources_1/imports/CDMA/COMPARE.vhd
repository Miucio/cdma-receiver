LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY COMPARE IS
	generic(
		Nbit  : integer
	);
	port(
		chip_stream    	 : in std_logic_vector(Nbit-1 downto 0);
		code_word    	 : in std_logic_vector(Nbit-1 downto 0);
		clk	    	 : in std_logic;
		res		 : in std_logic;
		bit_stream	 : out std_logic
	);
END COMPARE;
ARCHITECTURE bhCompare OF COMPARE IS
	signal counter   : std_logic_vector(15 downto 0); --sovradimensionato
BEGIN
process(clk,res)
begin
	
	if(res='1') then
		bit_stream <= '0';
		counter <= (OTHERS => '0');
	elsif(clk' event and clk='0') then
	   counter <= counter + '1';
	   if (counter > Nbit-2) then ---2 perch� deve entrare al 16 clock
	      counter <= (OTHERS => '0');
	      if chip_stream = code_word then
		bit_stream <= '1';
	      else
		bit_stream <= '0';
	      end if;
	   end if;
	end if;

end process;
END bhCompare;